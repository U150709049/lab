package drawing.shapes;

import drawingV4.Shape;

public class Circle extends Shape {

	protected int radius;

	public double area() {
		return Math.PI * radius * radius;
	}

}
