package alaa.shapes;

public class Circle {
	protected int radius;

	public Circle(int radius) {
		this.radius = radius;

	}

	public double area() {
		return 3.14 * radius * radius;

	}

}
