package exam;

public class Question7 {
	public static void main(String[] args) {
		System.out.println(powerN(5, 3));
	}

	public static int powerN(int n, int m) {
		if (m == 0) {
			return 1;
		} else {
			return n * powerN(n, m - 1);

		}
	}
}
