package exam.shapes;

public class Box extends Rectangle {

	int height;

	public Box(int w, int l, int h) {
		super(w, l);
		height = h;

	}

	public double area() {
		return super.area() * 6;

	}

}
