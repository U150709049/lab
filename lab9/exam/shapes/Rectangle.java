package exam.shapes;

public class Rectangle extends Shape {

	int width;
	int length;

	public Rectangle(int w, int l) {
		width = w;
		length = l;

	}

	public double area() {
		return width * length;

	}

}
