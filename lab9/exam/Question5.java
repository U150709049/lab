package exam;

public class Question5 {
	public static void main(String[] args) {

	}

	private static double calculateAverage(int[][] values) {
		double sum = 0;
		int count = 0;
		for (int i = 0; i < values.length; i++) {
			for (int j = 0; j < values.length; j++) {
				sum += values[i][j];
				count++;
			}
		}

		return sum / count;
	}

}
