package exam.animals;

public class Insect extends Animal {
	public String toString() {
		return "I am Insect " + super.toString();
	}

	public static String message() {
		return "Insect";
	}
}
