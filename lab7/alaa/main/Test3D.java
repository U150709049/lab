package alaa.main;

import java.util.ArrayList;

import alaa.shapes3d.Cube;
import alaa.shapes3d.Cylinder;

public class Test3D {

	public static void main(String[] args) {
		ArrayList<Cylinder> cylinders = new ArrayList<>();
		Cylinder cylinder = new Cylinder();

		cylinders.add(cylinder);
		cylinders.add(new Cylinder(6, 7));

		System.out.println(cylinders);

		ArrayList<Cube> cubes = new ArrayList<>();
		Cube cube = new Cube(0);

		cubes.add(cube);
		cubes.add(Cube(4));

		System.out.println(cubes);
	}

	private static Cube Cube(int i) {
		// TODO Auto-generated method stub
		return null;
	}

}
