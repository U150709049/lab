public class GCDRec {

	public static void main(String[] args) {

		int n1 = Integer.parseInt(args[0]);
		int n2 = Integer.parseInt(args[1]);
		int sum = sum(n1, n2);
		System.out.println(sum(n1, n2));
	}

	public static int sum(int n1, int n2) {

		if (n2 != 0)
			return sum(n2, n1 % n2);
		else
			return n1;
	}
}
